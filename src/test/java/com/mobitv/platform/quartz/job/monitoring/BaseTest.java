/**
 * Created by mobi-spring-boot-app-archetype.
 */
package com.mobitv.platform.quartz.job.monitoring;


import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mobitv.commons.health.service.impl.ZooKeeperHealthServiceImpl;
import com.mobitv.commons.servicediscovery.DiscoveryService;

/**
 * @author swapnil_mahale
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes={Application.class})
@ActiveProfiles({"test"})
public abstract class BaseTest {

protected static final HttpStatus OK = HttpStatus.OK;
	
	@MockBean
	protected DiscoveryService discoveryService;
	
	@MockBean
	protected ZooKeeperHealthServiceImpl zookeeperHealthService;
	
	@AfterClass
	public static void tearDown() throws ClientProtocolException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
	    HttpPut httpPut = new HttpPut("http://nocon01p1.ci-cp.dev.smf1.mobitv:8500/v1/agent/service/deregister/mobi-platform-quartz-job-monitoring-junit");
	    httpPut.setHeader("X-Consul-Token", "34a868a7-db53-68fa-8435-7b5f1b5268d1");
	    CloseableHttpResponse response = client.execute(httpPut);
	    assertEquals(200, response.getStatusLine().getStatusCode());
	    client.close();
	}
	
}
