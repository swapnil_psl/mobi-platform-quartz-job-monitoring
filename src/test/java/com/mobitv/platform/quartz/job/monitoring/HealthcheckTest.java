/**
 * Created by mobi-spring-boot-app-archetype.
 */
package com.mobitv.platform.quartz.job.monitoring;


import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


/**
 * @author swapnil_mahale
 * 
 * To run JUnits locally without connecting to VPN, rename src/test/resource/bootstrap.properties to some other name
 * and start consul agent locally.
 *
 */
public class HealthcheckTest extends BaseTest {

	private static final Logger LOGGER = Logger.getLogger(HealthcheckTest.class);
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void testMonitoringHealth() throws ClientProtocolException, IOException {
		ResponseEntity<String> healthResponse = restTemplate.getForEntity("/monitoring/health.json", String.class);
		HttpStatus actualStatusCode = healthResponse.getStatusCode();
		LOGGER.info("Monitoring Health Response: \n " + healthResponse.getBody());
		assertEquals("Application Monitoring Healthcheck", OK, actualStatusCode);
	}
	
	@Test
	public void testLoadbalancerHealth() {
		ResponseEntity<String> healthResponse = restTemplate.getForEntity("/loadbalancer/health.json", String.class);
		HttpStatus actualStatusCode = healthResponse.getStatusCode();
		LOGGER.info("Loadbalancer Health Response: \n " + healthResponse.getBody());
		assertEquals("Application Loadbalancer Healthcheck", OK, actualStatusCode);
	}
}
