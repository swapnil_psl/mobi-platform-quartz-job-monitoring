package com.mobitv.platform.quartz.job.monitoring.rest;



public final class Constants {
	public static final String SPLIT_REGEX = "\\s*,\\s*";
	public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	public static final String VERSION_SOLR_FIELD_NAME = "_version_";
	public static final String FACET_OFFSET = "facet.offset";
	public static final int MAX_FIELD_SIZE_FOR_ATOMIC_UPDATE = 5;
	public static final String SERIES_ROLLUP_JOB_NAME = "SERIES_ROLLUP_JOB";
	public static final String CHANNEL_ROLLUP_JOB_NAME = "CHANNEL_ROLLUP_JOB";
	public static final String VOD_ROLLUP_JOB_NAME = "VOD_ROLLUP_JOB";
	public static final String PROGRAM_ROLLUP_JOB_NAME = "PROGRAM_ROLLUP_JOB";
	public static final String SHARED_REF_ROLLUP_JOB_NAME = "SHARED_REF_ROLLUP_JOB";
	public static final String MISSING_DAM_DATA_REMOVAL_JOB_NAME = "MISSING_DAM_DATA_REMOVAL_JOB";
	public static final String POLICY_ROLLUP_JOB_NAME = "POLICY_ROLLUP_JOB";
	public static final String SERIES_ROLLUP_CRON_TRIGER_NAME = "SERIES_ROLLUP_CRON_TRIGER";
	public static final String CHANNEL_ROLLUP_CRON_TRIGER_NAME = "CHANNEL_ROLLUP_CRON_TRIGER";
	public static final String VOD_ROLLUP_CRON_TRIGER_NAME = "VOD_ROLLUP_CRON_TRIGER";
	public static final String PROGRAM_ROLLUP_CRON_TRIGER_NAME = "PROGRAM_ROLLUP_CRON_TRIGER";
	public static final String SHARED_REF_ROLLUP_CRON_TRIGER_NAME = "SHARED_REF_ROLLUP_CRON_TRIGER";
	public static final String MISSING_DAM_DATA_REMOVAL_TRIGGER_NAME = "MISSING_DAM_DATA_REMOVAL_CRON_TRIGGER";
	public static final String POLICY_ROLLUP_TRIGGER_NAME = "POLICY_ROLLUP_CRON_TRIGGER";
	public static final Long MAX_THREAD_POOL_WAIT = 10800L;
	
	public static final String PROGRAM_UPDATE_BY_CHANNEL_OPERATION_NAME = "rollupProgram";
	public static final String SERIES_UPDATE_BY_EXPIRY_OPERATION_NAME = "rollupSeriesForExpiredAssets";
	public static final String SERIES_DATE_FIELDS_OPERATION_NAME = "rollupSeriesDateFieldsForActiveVodAndPrograms";
	public static final String SERIES_CHILD_UPDATE_OPERATION_NAME = "rollupSeriesForUpdatedVodOrProgramsInSolr";
	public static final String SHARED_REF_UPDATE_OPERATION_NAME = "rollupSharedRef";
	public static final String VOD_UPDATE_OPERATION_NAME = "rollupVod";
	public static final String DELETE_EXPIRED_DOCS_OPERATION_NAME = "deleteExpiredDocs";
	public static final String POLICY_UPDATE_HANDLER_OPERATION_NAME = "policyUpdateHandler";
	public static final String CATCHUP_RESTRICTION_HANDLER_OPERATION_NAME = "catchupRestrictionHandler";
	public static final String ASSET_TYPE_PROGRAM = "program";
	public static final String ASSET_TYPE_VOD = "vod";
	public static final String ASSET_TYPE_CHANNEL = "channel";
	public static final String SOLR_SIMPLE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String SEPARATOR_COMMA = ",";
	public static final String SEPARATOR_SPACE = " ";
	
	private Constants() {
		
	}
	
}
