package com.mobitv.platform.quartz.job.monitoring.rest;

import java.util.List;
import java.util.Map;

import javax.print.attribute.standard.JobName;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.mobitv.exceptionwrapper.dto.ErrorWrapper;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.exceptionwrapper.exception.MobiInternalServerErrorException;
import com.mobitv.platform.quartz.job.monitoring.exception.ExceptionCode;

/**
 * @author sonal_lokhande
 *
 */
@Controller
@Path("/monitor/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class MonitoringEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringEndpoint.class);

	@Autowired
	private QuartzService quartzService;

	@GET
	@Path("status")
	public List<QuartzJobStatus> getStatus() {
		List<QuartzJobStatus> status;
		try {
			status = quartzService.getStatus();
		} catch (SchedulerException e) {
			LOGGER.error("Failed to get status of job.", e);
			throw new MobiInternalServerErrorException(
					ExceptionCode.INTERNAL_SERVER_ERROR.getCode(),
					ExceptionCode.INTERNAL_SERVER_ERROR.getMessage(),
					"Failed to get status of job.");
		}
		return status;
	}

	@POST
	@Path("disable")
	public List<QuartzJobStatus> disableJob(@QueryParam("job_name") String jobName) {
		try {
			quartzService.disableJob(jobName);
			return getStatus();
		} catch (SchedulerException e) {
			LOGGER.error("Failed to get status of job.", e);
			throw new MobiInternalServerErrorException(
					ExceptionCode.INTERNAL_SERVER_ERROR.getCode(),
					ExceptionCode.INTERNAL_SERVER_ERROR.getMessage(),
					"Failed to enable job.");
		}
	}
	
	@POST
	@Path("enable")
	public List<QuartzJobStatus> enableJob(@QueryParam("job_name") String jobName) {
		try {
			quartzService.enableJob(jobName);
			return getStatus();
		} catch (SchedulerException e) {
			LOGGER.error("Failed to get status of job.", e);
			throw new MobiInternalServerErrorException(
					ExceptionCode.INTERNAL_SERVER_ERROR.getCode(),
					ExceptionCode.INTERNAL_SERVER_ERROR.getMessage(),
					"Failed to enable job.");
		}
	}
	@PUT
    @Path("update")
    public List<QuartzJobStatus> updateJobData(@QueryParam("job_name") String jobName, @QueryParam("operation_name") String operationName,@QueryParam("start_time") String jobRunTimeString) {
		try{
			if(StringUtils.isBlank(operationName) || StringUtils.isBlank(jobName)) {
				LOGGER.error("Failed to get status of job.");
				throw new MobiException(HttpStatus.SC_BAD_REQUEST, new ErrorWrapper("","","Missing mandatory query parameter, operation_name"));
			}
			return quartzService.updateJobData(jobName, operationName,jobRunTimeString);
	
		}
		catch(Exception e)
		{
			LOGGER.error("Failed to get status of job.",e);
		}
		return null;
	}
}
