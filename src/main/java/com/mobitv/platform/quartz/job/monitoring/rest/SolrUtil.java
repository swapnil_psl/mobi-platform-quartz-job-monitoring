package com.mobitv.platform.quartz.job.monitoring.rest;

import static com.mobitv.platform.quartz.job.monitoring.rest.Constants.SIMPLE_DATE_FORMAT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
public final class SolrUtil {
	
	
	/**
	 * @param dateFormattedString
	 * @return
	 * @throws ParseException
	 */
	public static Date convertDateStringToDate(String dateFormattedString) throws ParseException {
		if(StringUtils.isNotBlank(dateFormattedString)) {
			SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_FORMAT);
			return sdf.parse(dateFormattedString);
		} else {
			return null;
		}
	}
	

}