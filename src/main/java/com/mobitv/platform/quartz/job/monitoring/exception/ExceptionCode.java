/**
 * 
 */
package com.mobitv.platform.quartz.job.monitoring.exception;

import org.springframework.http.HttpStatus;

public enum ExceptionCode {

	INVALID_UPSTREAM_SERVER_RESPONSE("Invalid response from the upstream server.", HttpStatus.BAD_GATEWAY),
	INVALID_JOB_NAME("Invalid Quartz Job Name", HttpStatus.BAD_REQUEST),
	PUBLISHER_DATA_LOCK("Publisher Job is Running", HttpStatus.LOCKED), 
	INTERNAL_SERVER_ERROR("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR),
	INVALID_ASSET_IDS("Asset Ids cannot be blank", HttpStatus.BAD_REQUEST)
	;
	
	private ExceptionCode(String message, HttpStatus httpStatus) {
		this.message = message;
		this.httpStatus = httpStatus;
	}
	
	private final String message;
	private final HttpStatus httpStatus;
	
	public String getCode() {
		return name();
	}
	
	public String getMessage() {
		return message;
	}
	
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	
}
