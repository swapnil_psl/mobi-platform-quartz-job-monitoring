/**
 * 
 */
package com.mobitv.platform.quartz.job.monitoring;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.mobitv.platform.quartz.job.monitoring.rest.QuartzJobConfig;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBeanBuilder;

/**
 * @author sonal_lokhande
 *
 */
@Component
public class QuartzJobConfigurationManager {

	public static final String SAMPLE_CSV_FILE_PATH = "src/main/resources/jobs.csv";

	private List<QuartzJobConfig> quartzJobsConfigurations;
	
	@PostConstruct
	public void init() throws IOException {
		try (Reader reader = Files.newBufferedReader(Paths
				.get(SAMPLE_CSV_FILE_PATH));) {
			ColumnPositionMappingStrategy<QuartzJobConfig> strategy = new ColumnPositionMappingStrategy<QuartzJobConfig>();
			strategy.setType(QuartzJobConfig.class);
			String[] memberFieldsToBindTo = { "schedName","name", "group" };
			strategy.setColumnMapping(memberFieldsToBindTo);

			quartzJobsConfigurations = new CsvToBeanBuilder(reader)
			.withMappingStrategy(strategy).withSkipLines(1)
					.withIgnoreLeadingWhiteSpace(true).build().parse();
		}
	}
	
	public List<QuartzJobConfig> getQuartzJobsConfigurations(){
		return quartzJobsConfigurations;
	}
	
	public QuartzJobConfig getQuartzJobConfig(String jobName){
		for (QuartzJobConfig quartzJobConfig : quartzJobsConfigurations) {
			if (quartzJobConfig.getName().equalsIgnoreCase(jobName)) {
				return quartzJobConfig;
			}
		}
		return null;
	}
}
