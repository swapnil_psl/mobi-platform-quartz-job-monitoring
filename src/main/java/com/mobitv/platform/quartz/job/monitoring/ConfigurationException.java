/**
 * Created by mobi-spring-boot-app-archetype.
 */
package com.mobitv.platform.quartz.job.monitoring;

public class ConfigurationException extends RuntimeException {

	private static final long serialVersionUID = -5070003292942755229L;
	
	public ConfigurationException(Throwable cause) {
		super(cause);
	}
	
	public ConfigurationException(String message) {
		super(message);
	}

}
