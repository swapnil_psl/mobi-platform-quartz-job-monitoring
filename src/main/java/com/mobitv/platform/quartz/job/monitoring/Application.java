/**
 * Created by mobi-spring-boot-app-archetype.
 */
package com.mobitv.platform.quartz.job.monitoring;

import org.springframework.boot.Banner.Mode;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author sonal_lokhande
 *
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).bannerMode(Mode.LOG)
				.run(args);
	}

}