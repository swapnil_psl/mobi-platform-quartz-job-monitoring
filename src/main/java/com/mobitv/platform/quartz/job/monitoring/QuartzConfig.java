package com.mobitv.platform.quartz.job.monitoring;

import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration("quartzConfig")
public class QuartzConfig {

	
    @Value("${database.driver_class}")
    private String driverClassName;

    @Value("${database.datasource.initialSize}")
    private int initialSize;

    @Value("${database.datasource.maxActive}")
    private int maxActive;

    @Value("${database.datasource.maxIdle}")
    private int maxIdle;

    @Value("${database.datasource.maxWait}")
    private long maxWait;

    @Value("${database.datasource.numTestsPerEvictionRun}")
    private int numTestsPerEvictionRun;

    @Value("${database.datasource.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;

    @Value("${database.datasource.validationQuery}")
    private String validationQuery;

    @Value("${quartz.database.url}")
    private String quartzDatabaseUrl;

    @Value("${quartz.database.username}")
    private String quartzDatabaseUsername;

    @Value("${quartz.database.password}")
    private String quartzDatabasePassword;

    @Bean(name = "qrtzDataSource")
    public BasicDataSource qrtzDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setInitialSize(initialSize);
        dataSource.setMaxActive(maxActive);
        dataSource.setMaxIdle(maxIdle);
        dataSource.setMaxWait(maxWait);
        dataSource.setUrl(quartzDatabaseUrl);
        dataSource.setUsername(quartzDatabaseUsername);
        dataSource.setPassword(quartzDatabasePassword);
        dataSource.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        dataSource.setValidationQuery(validationQuery);
        dataSource.setRemoveAbandoned(true);
        dataSource.setDefaultAutoCommit(true);
        return dataSource;
    }

    @Bean(name = "hibernateProperties")
    public Properties hibernateProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/hibernate.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }

    @Bean(name = "qrtzSessionFactory")
    public SessionFactory qrtzSessionFactory() throws IOException {
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(qrtzDataSource());
        localSessionFactoryBean.setPackagesToScan("com.mobitv.cms");
        localSessionFactoryBean.setHibernateProperties(hibernateProperties());
        localSessionFactoryBean.afterPropertiesSet();
        return localSessionFactoryBean.getObject();
    }

    @Value("${quartz.startup.delay}")
    private int startupDelay;

    @Value("${quartz.auto.startup}")
    private boolean autoStartup;

    @Value("${quartz.waitcomplete.shutdown}")
    private boolean waitCompleteShutdown;

    @Value("${quartz.overwrite.existingjob}")
    private boolean overwriteExistingJob;
    
    @Bean(name = "quartzProperties")
    public Properties quartzProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }

    @Bean(name ="publishAssetSchedulerFactory")
    public SchedulerFactoryBean publishAssetSchedulerFactory() throws IOException, ParseException {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setStartupDelay(startupDelay);
        schedulerFactoryBean.setAutoStartup(autoStartup);
        schedulerFactoryBean.setWaitForJobsToCompleteOnShutdown(waitCompleteShutdown);
        schedulerFactoryBean.setDataSource(qrtzDataSource());
        schedulerFactoryBean.setOverwriteExistingJobs(overwriteExistingJob);
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
        schedulerFactoryBean.setQuartzProperties(quartzProperties());
        return schedulerFactoryBean;
    }

    @Bean(name = "rollupServiceSchedulerFactory")
    public SchedulerFactoryBean rollupServiceSchedulerFactory() throws IOException, ParseException {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setStartupDelay(startupDelay);
        schedulerFactoryBean.setAutoStartup(autoStartup);
        schedulerFactoryBean.setWaitForJobsToCompleteOnShutdown(waitCompleteShutdown);
        schedulerFactoryBean.setDataSource(qrtzDataSource());
        schedulerFactoryBean.setOverwriteExistingJobs(overwriteExistingJob);
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
        schedulerFactoryBean.setQuartzProperties(quartzProperties());
        return schedulerFactoryBean;
    }
    
    @Bean(name ="publishOfferSchedulerFactory") 
    public SchedulerFactoryBean publishOfferSchedulerFactory() throws IOException, ParseException {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setStartupDelay(startupDelay);
        schedulerFactoryBean.setAutoStartup(autoStartup);
        schedulerFactoryBean.setWaitForJobsToCompleteOnShutdown(waitCompleteShutdown);
        schedulerFactoryBean.setDataSource(qrtzDataSource());
        schedulerFactoryBean.setOverwriteExistingJobs(overwriteExistingJob);
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
        schedulerFactoryBean.setQuartzProperties(quartzProperties());
        return schedulerFactoryBean;
    }

    @Bean(name = "publishPolicySchedulerFactory") 
    public SchedulerFactoryBean publishPolicySchedulerFactory() throws IOException, ParseException {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setStartupDelay(startupDelay);
        schedulerFactoryBean.setAutoStartup(autoStartup);
        schedulerFactoryBean.setWaitForJobsToCompleteOnShutdown(waitCompleteShutdown);
        schedulerFactoryBean.setDataSource(qrtzDataSource());
        schedulerFactoryBean.setOverwriteExistingJobs(overwriteExistingJob);
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
        schedulerFactoryBean.setQuartzProperties(quartzProperties());
        return schedulerFactoryBean;
    }
    
}
