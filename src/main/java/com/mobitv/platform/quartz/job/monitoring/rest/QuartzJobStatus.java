package com.mobitv.platform.quartz.job.monitoring.rest;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sonal_lokhande
 *
 */
@XmlRootElement(name="QuartzJobStatus")
@XmlAccessorType(XmlAccessType.FIELD)
public class QuartzJobStatus {
	
	@XmlElement(name="jobName")
	private String jobName;
	@XmlElement(name="jobGroup")
	private String jobGroup;
	@XmlElement(name="jobStatus")
	private String jobStatus;
	@XmlElement(name="jobData")
	private Map<String,Object> jobData;
	
	
	public QuartzJobStatus() {
		
	}
	
	public QuartzJobStatus(String jobName, String jobGroup, Map<String,Object> operationList,String jobStatus) {
		super();
		this.jobName = jobName;
		this.jobGroup = jobGroup;
		this.jobStatus = jobStatus;
		this.jobData= operationList;
	}



	public Map<String, Object> getOperationList() {
		return jobData;
	}

	public void setOperationList(Map<String, Object> operationList) {
		this.jobData = operationList;
	}

	public String getJobName() {
		return jobName;
	}


	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	public String getJobGroup() {
		return jobGroup;
	}


	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}


	public String getJobStatus() {
		return jobStatus;
	}


	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	@Override
	public String toString() {
		return "QuartzJobStatus [jobName=" + jobName + ", jobGroup=" + jobGroup
				+ ", jobStatus=" + jobStatus + ", operationList="
				+ jobData + "]";
	}


	

	
}
