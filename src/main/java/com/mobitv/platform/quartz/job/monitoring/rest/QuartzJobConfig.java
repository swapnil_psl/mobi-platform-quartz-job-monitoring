package com.mobitv.platform.quartz.job.monitoring.rest;

public class QuartzJobConfig {

	private String schedName;
	private String name;
	private String group;
	
	public QuartzJobConfig() { }
	
	public String getSchedName() {
		return schedName;
	}

	public void setSchedName(String schedName) {
		this.schedName = schedName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "QuartzJobConfig [schedName=" + schedName + ", name=" + name
				+ ", group=" + group + "]";
	}
	
}

