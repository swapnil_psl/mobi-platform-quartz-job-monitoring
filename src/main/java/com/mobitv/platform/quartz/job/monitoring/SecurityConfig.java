package com.mobitv.platform.quartz.job.monitoring;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
/*import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.authentication.encoding.LdapShaPasswordEncoder;
*/
/**
 * @author sonal_lokhande
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter
	{

	/* protected void configure(HttpSecurity http) throws Exception {
		    http
		      .authorizeRequests()
		        .anyRequest().fullyAuthenticated()
		        .and()
		      .formLogin();
		  }

	 @Override
	 public void configure(AuthenticationManagerBuilder auth) throws Exception {
		    auth
		      .ldapAuthentication()
		        .userDnPatterns("uid={0},ou=people")
		        .groupSearchBase("ou=groups")
		        .contextSource()
		          .url("ldap://localhost:8389/dc=springframework,dc=org")
		          .and()
		        .passwordCompare()
		          .passwordEncoder(new LdapShaPasswordEncoder())
		          .passwordAttribute("userPassword");
		  }*/
	

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
        .withUser("Quartz_Admin").password("mobi_jobs").roles("ADMIN");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic()
                .and()
                .authorizeRequests()
              	.antMatchers(HttpMethod.POST, "/monitor/enable").hasRole("ADMIN")
              	.antMatchers(HttpMethod.POST, "/monitor/disable").hasRole("ADMIN")
              	.antMatchers(HttpMethod.PUT, "/monitor/update").hasRole("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }

	
	}
