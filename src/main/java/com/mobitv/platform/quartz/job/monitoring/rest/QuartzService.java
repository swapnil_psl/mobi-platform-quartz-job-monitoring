package com.mobitv.platform.quartz.job.monitoring.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.mobitv.exceptionwrapper.exception.MobiBadRequestException;
import com.mobitv.exceptionwrapper.exception.MobiInternalServerErrorException;
import com.mobitv.platform.quartz.job.monitoring.QuartzJobConfigurationManager;
import com.mobitv.platform.quartz.job.monitoring.exception.ExceptionCode;


/**
 * @author sonal_lokhande
 *
 */
@Service
public class QuartzService {
	private static final Logger logger = LoggerFactory.getLogger(MonitoringEndpoint.class);
	public static final String SERIES_ROLLUP_JOB_NAME = "SERIES_ROLLUP_JOB";
	public static final String CHANNEL_ROLLUP_JOB_NAME = "CHANNEL_ROLLUP_JOB";
	public static final String VOD_ROLLUP_JOB_NAME = "VOD_ROLLUP_JOB";
	public static final String PROGRAM_ROLLUP_JOB_NAME = "PROGRAM_ROLLUP_JOB";
	public static final String SHARED_REF_ROLLUP_JOB_NAME = "SHARED_REF_ROLLUP_JOB";
	public static final String MISSING_DAM_DATA_REMOVAL_JOB_NAME = "MISSING_DAM_DATA_REMOVAL_JOB";
	public static final String POLICY_ROLLUP_JOB_NAME = "POLICY_ROLLUP_JOB";


	@Autowired
	@Qualifier(value = "publishAssetSchedulerFactory")
	private SchedulerFactoryBean publishAssetSchedulerFactory;
	@Autowired
	@Qualifier(value = "publishOfferSchedulerFactory")
	private SchedulerFactoryBean publishOfferSchedulerFactory;
	@Autowired
	@Qualifier(value = "publishPolicySchedulerFactory")
	private SchedulerFactoryBean publishPolicySchedulerFactory;
	@Autowired
	@Qualifier(value = "rollupServiceSchedulerFactory")
	private SchedulerFactoryBean rollupServiceSchedulerFactory;

	@Autowired
	private QuartzJobConfigurationManager quartzJobConfigurationManager;

	private static Map<String, SchedulerFactoryBean> schedulerNameToSchedulerFactoryBeanMap = new HashMap<>();

	@PostConstruct
	public void init() {
		schedulerNameToSchedulerFactoryBeanMap.put(
				"publishAssetSchedulerFactory", publishAssetSchedulerFactory);
		schedulerNameToSchedulerFactoryBeanMap.put(
				"publishOfferSchedulerFactory", publishOfferSchedulerFactory);
		schedulerNameToSchedulerFactoryBeanMap.put(
				"publishPolicySchedulerFactory", publishPolicySchedulerFactory);
		schedulerNameToSchedulerFactoryBeanMap.put(
				"rollupServiceSchedulerFactory", rollupServiceSchedulerFactory);
	}

	public List<QuartzJobStatus> getTriggerStatus(QuartzJobConfig quartzJobConfig) throws SchedulerException {

		List<QuartzJobStatus> jobStatus = new ArrayList<QuartzJobStatus>();
		JobKey jobKey = new JobKey(quartzJobConfig.getName(),quartzJobConfig.getGroup());

		SchedulerFactoryBean schedulerFactory = schedulerNameToSchedulerFactoryBeanMap.get(quartzJobConfig.getSchedName());
		Scheduler scheduler = schedulerFactory.getScheduler();

		List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);

		if (CollectionUtils.isNotEmpty(triggers)) {
			Map<String, Object> keyValues = null;
			for (Trigger trigger : triggers) {
				TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
				keyValues = new HashMap<>();
				JobDetail mainJobDetail = scheduler.getJobDetail(jobKey);
				JobDataMap mainJobDataMap = mainJobDetail.getJobDataMap();
				Set<Entry<String, Object>> allData = mainJobDataMap.entrySet();
				for (Entry<String, Object> entry : allData) {
					keyValues.put(entry.getKey(), entry.getValue());
				}
						
				jobStatus.add(new QuartzJobStatus(quartzJobConfig.getName(),
						quartzJobConfig.getGroup(), keyValues, triggerState
								.name()));
			}
		}
		return jobStatus;
	}

	public List<QuartzJobStatus> getStatus() throws SchedulerException {
		List<QuartzJobStatus> allJobStatus = new ArrayList<>();
		List<QuartzJobConfig> quartzJobs = quartzJobConfigurationManager.getQuartzJobsConfigurations();
		for (QuartzJobConfig quartzJob : quartzJobs) {
			allJobStatus.addAll(getTriggerStatus(quartzJob));
		}
		return allJobStatus;
	}
	
	public void disableJob(String jobName)throws SchedulerException {
		QuartzJobConfig quartzJobConfig = quartzJobConfigurationManager.getQuartzJobConfig(jobName);
		if (quartzJobConfig == null) {
			throw new MobiBadRequestException(ExceptionCode.INVALID_JOB_NAME.getCode(), ExceptionCode.INVALID_JOB_NAME.getMessage(), 
					"Invalid job_name query parametrs");
		}
		SchedulerFactoryBean schedulerFactory = schedulerNameToSchedulerFactoryBeanMap.get(quartzJobConfig.getSchedName());
		Scheduler scheduler = schedulerFactory.getScheduler();
		JobKey jobKey = new JobKey(quartzJobConfig.getName(),quartzJobConfig.getGroup());
		scheduler.pauseJob(jobKey);
	}
	
	public void enableJob(String jobName)throws SchedulerException {
		QuartzJobConfig quartzJobConfig = quartzJobConfigurationManager.getQuartzJobConfig(jobName);
		if (quartzJobConfig == null) {
			throw new MobiBadRequestException(ExceptionCode.INVALID_JOB_NAME.getCode(), ExceptionCode.INVALID_JOB_NAME.getMessage(), 
					"Invalid job_name query parametrs");
		}
		SchedulerFactoryBean schedulerFactory = schedulerNameToSchedulerFactoryBeanMap.get(quartzJobConfig.getSchedName());
		Scheduler scheduler = schedulerFactory.getScheduler();
		JobKey jobKey = new JobKey(quartzJobConfig.getName(),quartzJobConfig.getGroup());
		scheduler.resumeJob(jobKey);
	}

	public List<QuartzJobStatus> updateJobData(String jobName, String operationName, String jobRunTimeString) {
		logger.info("setting last run time for job [ {} ] to [ {} ]", jobName + " : " + operationName, jobRunTimeString);
		try {
			if(StringUtils.isNotBlank(jobRunTimeString) && StringUtils.isNotBlank(jobName)) 
			{
				Date newDate = SolrUtil.convertDateStringToDate(jobRunTimeString);
				return updateJobData(jobName, operationName, newDate);
			}
		} catch (Exception e) {
			throw new MobiInternalServerErrorException(ExceptionCode.PUBLISHER_DATA_LOCK.getCode(), "Failed to update job data", "Failed to update job data, please check logs for more details.");
		}
		return null;
			}
	
	public List<QuartzJobStatus> updateJobData(String jobName, String operationName, Date jobRunTime) throws SchedulerException {
		logger.info("setting last run time for job [ {} ] to [ {} ]", jobName + " : " + operationName, jobRunTime);
	
		if(jobRunTime != null && StringUtils.isNotBlank(jobName)){
			QuartzJobConfig quartzJobConfig = quartzJobConfigurationManager.getQuartzJobConfig(jobName);
			SchedulerFactoryBean schedulerFactory = schedulerNameToSchedulerFactoryBeanMap.get(quartzJobConfig.getSchedName());
			Scheduler scheduler = schedulerFactory.getScheduler();
			JobKey mainJobKey = new JobKey(quartzJobConfig.getName(),quartzJobConfig.getGroup());
			JobDetail mainJobDetail = scheduler.getJobDetail(mainJobKey);
			JobDataMap mainJobDataMap = mainJobDetail.getJobDataMap();
			Object oldDateObj = mainJobDataMap.put(operationName, jobRunTime);
			logger.info("previous last run time for job [ {} ] was [ {} ]",
					jobName + " : " + operationName, oldDateObj);
			scheduler.addJob(mainJobDetail, true);
		return	getTriggerStatus(quartzJobConfig);
		}
		return null;
		}
}
