# MobiTV Platform Quartz Job Monitoring

## APIs

### Get Job Status
#### Description

#### Suggestions



### Enable Job Status
#### Description
#### Suggestions

### Disable Job Status
#### Description
#### Suggestions

### Update Job Data
#### Description
#### Suggestions

## Improvements / Bugs
1. Fix all issues reported by Sonar.
2. Add loggers.
3. Add JavaDoc for each method.

## Enhancements
1. Remove dependency of CSV file. Read all jobs configuration from database.
2. New API : If status is BLOCKED, then on giving job name, fix that job. (removing entry from fired_trigger, change job status to WAITING, update next_fire_time)
